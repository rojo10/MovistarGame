import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/observable';
import { of } from 'rxjs/observable/of';
import { Card } from './memory-game.model';

@Injectable()
export class CardsService {

    i:number=0;
    cards:Card[];
    counter:number=0;
    cardsNew:[]=[];
    temp: any;
    index: any;
    getCards(): Observable<Card[]> {

        this.cards = [
            {
                name: 'php',
                img: 'assets/images/Emprende_juego_01.png',
                id: 1,
            },
            {
                name: 'css3',
                img: 'assets/images/Emprende_juego_02.png',
                id: 2
            },
            {
                name: 'html5',
                img: 'assets/images/Emprende_juego_03.png',
                id: 3
            },
            {
                name: 'jquery',
                img: 'assets/images/Emprende_juego_04.png',
                id: 4
            },
            {
                name: 'javascript',
                img: 'assets/images/Emprende_juego_05.png',
                id: 5
            },
            {
                name: 'node',
                img: 'assets/images/Emprende_juego_06.png',
                id: 6
            },
            {
                name: 'photoshop',
                img: 'assets/images/Emprende_juego_07.png',
                id: 7
            },
            {
                name: 'python',
                img: 'assets/images/Emprende_juego_08.png',
                id: 8
            },
            {
                name: 'rails',
                img: 'assets/images/Emprende_juego_09.png',
                id: 9
            },
            {
                name: 'sass',
                img: 'assets/images/Emprende_juego_10.png',
                id: 10
            },
            {
                name: 'sublime',
                img: 'assets/images/Emprende_juego_11.png',
                id: 11
            },
            {
                name: 'wordpress',
                img: 'assets/images/Emprende_juego_12.png',
                id: 12
            },
            {
                name: 'wordpress',
                img: 'assets/images/Emprende_juego_13.png',
                id: 12
            },
            {
                name: 'wordpress',
                img: 'assets/images/Emprende_juego_14.png',
                id: 12
            },
            {
                name: 'wordpress',
                img: 'assets/images/Emprende_juego_15.png',
                id: 12
            },
            {
                name: 'wordpress',
                img: 'assets/images/Emprende_juego_16.png',
                id: 12
            },
            {
                name: 'wordpress',
                img: 'assets/images/Emprende_juego_17.png',
                id: 12
            },
        ];


        return of(this.cards)
            .map(this.shuffle2(this.cards))
            .map(this.add)
            .map(this.duplicate)
            .map(this.shuffle);
    }

    add(cards: Card[]): Card[] {
        return cards.map(
            card => Object.assign({}, card, {
                picked: false,
                matched: false
            }));
    }

    duplicate(cards: Card[]): Card[] {
        return cards.concat(JSON.parse(JSON.stringify(cards)));
    }

    // Fisher--Yates Algorithm -- http://bost.ocks.org/mike/shuffle/
    shuffle(cards: Card[]): Card[] {
        this.counter = cards.length;
        console.log(this.counter);
        this.cards=[];
        // While there are elements in the array
        while (this.counter > 0) {
            // Pick a random index
            this.index = Math.floor(Math.random() * this.counter);
            // Decrease counter by 1
            this.counter--;
            // And swap the last element with it
            this.temp = this.cards[counter];
            this.cards[counter] = this.cards[this.index];
            this.cards[index] = this.temp;
        }
        return this.cards;
    }

    shuffle2(cards: Card[]): Card[] {
        this.counter = cards.length;



        while (this.counter > 0) {
            this.index = Math.floor(Math.random() * this.counter);
            this.counter--;

            this.temp = this.cards[this.counter];
            this.cards[this.counter] = this.cards[this.index];
            this.cards[this.index] = this.temp;
        }

        for (this.i = 0; this.i < 3; this.i++) {
            this.cardsNew[this.i] = this.cards[this.i];
        }
        return this.cardsNew;
    }


}
