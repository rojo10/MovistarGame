import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {MemoryGameComponent} from '../features/memory-game/memory-game.component';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController) {

  }

}
